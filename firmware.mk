# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/gauguin-firmware/abl_ecc.elf:install/firmware-update/abl_ecc.elf \
    vendor/xiaomi/gauguin-firmware/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi/gauguin-firmware/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi/gauguin-firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi/gauguin-firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi/gauguin-firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi/gauguin-firmware/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi/gauguin-firmware/featenabler.mbn:install/firmware-update/featenabler.mbn \
    vendor/xiaomi/gauguin-firmware/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi/gauguin-firmware/km4.mbn:install/firmware-update/km4.mbn \
    vendor/xiaomi/gauguin-firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi/gauguin-firmware/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi/gauguin-firmware/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi/gauguin-firmware/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi/gauguin-firmware/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
    vendor/xiaomi/gauguin-firmware/xbl.elf:install/firmware-update/xbl.elf \
    vendor/xiaomi/gauguin-firmware/xbl_config.elf:install/firmware-update/xbl_config.elf
